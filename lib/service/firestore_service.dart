import 'package:cloud_firestore/cloud_firestore.dart';

class FirestoreService {
  final CollectionReference _collectionReference;

  FirestoreService(this._collectionReference);
  // FirebaseFirestore.instance.collection('mbindane');

  // Create a new document
  Future<void> createDocument(Map<String, dynamic> data) async {
    await _collectionReference.add(data);
  }

  // Read all documents in the collection
  Stream<QuerySnapshot> getDocuments() {
    return _collectionReference.snapshots();
  }

  // Update a document
  Future<void> updateDocument(
      String documentId, Map<String, dynamic> data) async {
    await _collectionReference.doc(documentId).update(data);
  }

  // Delete a document
  Future<void> deleteDocument(String documentId) async {
    await _collectionReference.doc(documentId).delete();
  }

  // Read documents where name matches a specific value
  Future<List<DocumentSnapshot>> getDocumentsByName(String name) async {
    final QuerySnapshot snapshot =
        await _collectionReference.where('name', isEqualTo: name).get();
    return snapshot.docs;
  }
}
