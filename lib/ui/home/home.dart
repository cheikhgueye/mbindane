import 'package:flutter/material.dart';
import 'package:mbindane/widgets/CleaningCard.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Mbindane'),
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Femmes de travailleuses disponibles près de chez vous',
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 16.0),
            Expanded(
              child: ListView(
                children: [
                  CleaningCard(
                    name: 'Marie Dupont',
                    description: 'Expérience : 5 ans',
                    imageUrl: 'assets/bonn.jpeg',
                    rating: 4.5,
                    numReviews: 10,
                    isAvailable: true,
                  ),
                  CleaningCard(
                    name: 'Sophie Martin',
                    description: 'Expérience : 3 ans',
                    imageUrl: 'assets/bonn.jpeg',
                    rating: 3.8,
                    numReviews: 6,
                    isAvailable: false,
                  ),
                  // Ajoutez d'autres cartes pour d'autres femmes de ménage
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
