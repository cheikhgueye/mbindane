import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  bool isAvailable = false;

  void toggleAvailability() {
    setState(() {
      isAvailable = !isAvailable;
    });
  }

// String imageText = await pickImage();
//                   await saveImageToFirestore(imageText);
  Uint8List decodeImage(String imageText) {
    List<int> bytes = base64Decode(imageText);
    return Uint8List.fromList(bytes);
  }

  Future<String> pickImage() async {
    final ImagePicker _picker = ImagePicker();
    final XFile? pickedFile =
        await _picker.pickImage(source: ImageSource.gallery);

    if (pickedFile != null) {
      // Convertir l'image en bytes
      List<int> imageBytes = await pickedFile.readAsBytes();
      // Convertir les bytes en base64
      String base64Image = base64Encode(imageBytes);
      return base64Image;
    }

    return '';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Mon Profil'),
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CircleAvatar(
              radius: 60,
              backgroundImage: AssetImage('assets/bonn.jpeg'),
            ),
            SizedBox(height: 16.0),
            Text(
              'Sophie Martin',
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 8.0),
            Text(
              'Expérience : 3 ans',
              style: TextStyle(fontSize: 18),
            ),
            SizedBox(height: 16.0),
            Row(
              children: [
                Icon(Icons.calendar_today),
                SizedBox(width: 8.0),
                Text(
                  'Disponibilité',
                  style: TextStyle(fontSize: 18),
                ),
                SizedBox(width: 8.0),
                Switch(
                  value: isAvailable,
                  onChanged: (value) {
                    toggleAvailability();
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
