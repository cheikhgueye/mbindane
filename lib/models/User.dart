import 'dart:convert';

class UserModel {
  int id;
  String name;
  String desc;
  String img;
  double rating;
  bool isAvailable;
  UserModel(
      {this.id = 0,
      this.name = "",
      this.desc = "",
      this.img = "",
      this.isAvailable = false,
      this.rating = 0.0});
  UserModel.fromJson(Map<String, dynamic> map)
      : id = map['id'] ?? 0,
        name = map['name'] ?? "",
        desc = map['desc'] ?? "",
        img = map['img'] ?? "",
        isAvailable = map['isAvailable'] ?? false,
        rating = map["rating"] ?? 0.0;

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "desc": desc,
        "img": img,
        "isAvailable": isAvailable,
        "rating": rating
      };
  static List<UserModel> fromJsonList(List list) {
    if (list == null) return [];
    return list.map((item) => UserModel.fromJson(item)).toList();
  }
}
