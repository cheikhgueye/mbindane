import 'package:flutter/material.dart';

class CleaningCard extends StatelessWidget {
  final String name;
  final String description;
  final String imageUrl;
  final double rating;
  final int numReviews;
  final bool isAvailable;

  CleaningCard({
    required this.name,
    required this.description,
    required this.imageUrl,
    required this.rating,
    required this.numReviews,
    required this.isAvailable,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.asset(
            imageUrl,
            width: double.infinity,
            height: 200,
            fit: BoxFit.cover,
          ),
          Padding(
            padding: EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  name,
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 8.0),
                Text(
                  description,
                  style: TextStyle(fontSize: 16),
                ),
                SizedBox(height: 16.0),
                Row(
                  children: [
                    Icon(Icons.call),
                    SizedBox(width: 8.0),
                    Icon(Icons.videocam),
                    SizedBox(width: 8.0),
                    Icon(Icons.map),
                    SizedBox(width: 8.0),
                    Icon(Icons.stars),
                    SizedBox(width: 8.0),
                    Text(
                      '$rating ($numReviews)',
                      style: TextStyle(fontSize: 16),
                    ),
                    SizedBox(width: 8.0),
                    Icon(
                      isAvailable ? Icons.check_circle : Icons.cancel,
                      color: isAvailable ? Colors.green : Colors.red,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
